#!/usr/bin/gnuplot
#
# Plot trigonometric functions with Gnuplot.


reset

# wxt
#set terminal wxt size 450,262 enhanced font 'Verdana,10' persist
# png
#set terminal pngcairo size 1280,720 enhanced font 'Verdana,10'
#set output 'plotting_functions.png'
# svg
set terminal svg size 450,262 fname 'Verdana, Helvetica, Arial, sans-serif' \
fsize '10'
set output '2.svg'

# Line styles
set border linewidth 1.5
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2  # blue
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2  # red
set style line 3 linecolor rgb '#006600' linetype 1 linewidth 2  # green
# Legend
#set key at 6.1,1.3
# Axes label
set xlabel 'relative distance'
set ylabel 'weight'


# Axis ranges
set xrange[-1:1]
set yrange[0:1]
# Axis labels
#set xtics 3
#set ytics 1
#set tics scale 0.75
# Functions to plot
f(x) = (1- abs(x) )
g(x) = ((1- abs(x) )*((1- abs(x) )))
k(x) = ((1- abs(x) )*((1- abs(x) ))*((1- abs(x) )))
# Plot
plot f(x) title 'linear kernel' with lines ls 1, \
     g(x) title 'quadratic kernel' with lines ls 2, \
     k(x) title 'cubic kernel' with lines ls 3
