const std = @import("std");
const c = @cImport({
    @cInclude("sys/socket.h");
    @cInclude("sys/types.h");
    @cInclude("netinet/in.h");
    @cInclude("unistd.h");
    @cInclude("stdio.h");
    @cInclude("errno.h");
});

pub fn main() !void {
    const port = 8082;

    const socket_fd = c.socket(c.AF_INET, c.SOCK_STREAM, 0);
    if (socket_fd < 0) {
        std.log.err("Could not open servers socket.", .{});
        std.os.exit(1);
    }
    defer _ = c.close(socket_fd);

    const in_any = c.struct_in_addr{
        .s_addr = c.INADDR_ANY,
    };
    const serv_addr = c.sockaddr_in{
        .sin_family = c.AF_INET,
        .sin_port = c.htons(port),
        .sin_addr = in_any,
        .sin_zero = std.mem.zeroes([8]u8),
    };

    const size = @sizeOf(@TypeOf(serv_addr));

    var optval: i32 = 1;
    if (c.setsockopt(
        socket_fd,
        c.SOL_SOCKET,
        c.SO_REUSEADDR,
        @ptrCast(&optval),
        @sizeOf(@TypeOf(optval)),
    ) < 0) {
        std.log.err("Could not set options", .{});
        std.os.exit(1);
    }

    if (c.bind(socket_fd, @ptrCast(&serv_addr), size) < 0) {
        std.log.err("Serves socket could not bind to port {}", .{port});
        std.os.exit(1);
    }

    if (c.listen(socket_fd, 5) < 0) {
        std.log.err("Listening was not successful", .{});
        std.os.exit(1);
    }

    var cli_adr = std.mem.zeroes(c.sockaddr_in);
    var clilen: c.socklen_t = @sizeOf(@TypeOf(cli_adr));
    var new_fd = c.accept(socket_fd, @ptrCast(&cli_adr), @ptrCast(&clilen));
    defer _ = c.close(new_fd);

    if (new_fd < 0) {
        std.log.err("Could not accept incoming connection", .{});
        std.os.exit(1);
    }

    var buff = std.mem.zeroes([256]u8);
    var n = c.read(new_fd, @ptrCast(&buff), 255);
    if (n < 0) {
        std.log.err("Could not reach from buffer.", .{});
        std.os.exit(1);
    }

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    try stdout.print("{s}\n", .{buff});
    try bw.flush();

    const ret =
        \\HTTP/1.1 200 OK
        \\Server: Rusty ZIG server
        \\Content-Length: 5
        \\Content-Type: text/html
        \\Connection: closed
        \\
        \\hello
    ;
    if (c.write(new_fd, ret.ptr, ret.len) < 0) {
        std.log.err("Could not reach from buffer.", .{});
        std.os.exit(1);
    }
}
