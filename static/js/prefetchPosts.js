var support = function support(feature){
    var fakeLink = document.createElement('link');
    try {
        if(fakeLink.relList && _.isFunction(fakeLink.relList.supports)){
            return  fakeLink.relList.supports(feature);
        }
    } catch(err){
        return false;
    }
};

function prefetch(url){
    if ( support('prefetch')) {
        const link = document.createElement('link');
        link.rel = 'prefetch';
        link.href = url;
        link.as = 'document'
        document.head.appendChild(link);
      } else {
        fetch(url);
      }
}

const observer = new IntersectionObserver( (entries) => {
    for (const entry of entries) {
        if( entry.isIntersecting){
            const url = entry.target.getAttribute('href');
            prefetch(url);
            observer.unobserve(entry.target);
        }
    }
});

document.querySelectorAll('a.post-excerpt').forEach(link => {
    observer.observe(link);
  });