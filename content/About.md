---
layout: page
title: About
permalink: /about/
modified: 2024-02-04T18:30:58.602Z
gallery: true
---
<style type="text/css">
body{
  padding-right:0.9em;
}
</style>




<figure class="left third">
<div class="md__image">
    <img id="[1]" src="/res/photos/ava1_small.jpg" onclick="openModal(this.id)" alt="">
</div>
</figure>
My name is Jan Scheffczyk, and I currently work at the Fraunhofer Institute. In my team, we use natural language processing and machine learning to build tools that aid in the creation and maintenance of project requirements. I enjoy machine learning, but I'm especially passionate about building robust services that deliver our machine learning models to customers. With my background in computer graphics, I'm comfortable with low-level programming and know how to layout data in memory to improve performance. If we run into a performance bottleneck, I'm happy to dive headfirst into the nitty-gritty details until the program runs fast enough.

During my time at university FH Bingen I developed an interest in Computer Graphics which most likely stems from my affection towards video games. This interest lead me to also venture into physically based pbrt rendering as well as simulation or rather approximation of physical interactions.

I use this website to present the fruits of my endeavours and hopefully offer those with a similar interest
a useful insight into my thought process. Further i aim to give an overview of the key-techniques used in each project
and highlight the aspects i struggled with to allow interested readers to more easily understand the cited
references.

