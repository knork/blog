---
layout: post
title:  "Bachelor: Interactive simulation of anisotropic fracturing with position based composite materials"
date:   2018-09-01 00:28:35 +0100
description: "A novel approach that simulates transversely isotropic materials by combining rod and solid simulations, mimicking timber's structure. It allows for flexible shaping and intuitive control over the direction of anisotropy, enabling the simulation of various materials. While this method offers real-time interaction, it compromises on physical accuracy."

permalink: bachelor.html
---
<figure >
<a class="max-width imgbox" href="/res/photos/pbrt_6_4/bokeh_init.jpeg" data-imagelightbox="a">
<img  src="/res/photos/bachelor/main.png" >
</a>
<figurecaption>Simulation of wood fracture</figurecaption>
</figure>

[Full Thesis](/res/pdf/bachelor.pdf)

# Abstract
In this bachelor thesis we present a novel approach for simulating transverse-
isotropic materials. In contrast to classical approaches that integrate the
anisotropy into the strain-stress-constitutive model the presented approach
is inspired by the natural structure of timber. A rod simulation that repre-
sents the fibers in timber is combined with a solid simulation that represents
the cementing material to create an anisotropic composite material. For this
purpose numerous approaches to calculate the dynamic response of rods and
solids as well as their common theoretical background, continuum mechanics,
are reviewed. By utilizing mesh-less solid simulation and scattered rods to
discretize the object, our approach is not limited to a specific shape. Further
by specifying the direction of anisotropy for each particle in the object results
in a particular rod pattern. Uniform anisotropic directions create fibrous ma-
terial like wood while random directions create locally anisotropic materials
like press wood. Modulating the randomness allows to simulate a wide vari-
ety of materials. In addition, specifying the direction of anisotropy is more
intuitive than parametrization of the stress-strain-constitutive model. The
approach extends the recently popularized position based dynamics (PBD)
allowing interactive frame rates at the cost of poor convergence making it
difficult to achieve physically meaningful material parameters.






