---
layout: post
title:  "Data Oriented Design"
date:   2016-02-08 00:28:35 +0100
description: "Short Essay on Data Oriented Design focusing on CPU-Cache utilization. Furthermore some downsides of the most eminent and thought programming paradigm Object-Oriented-Programming are investigated"

---

Within the last few decades CPU performance doubled almost annually, leav-
ing the memory performance lacking behind.  While new memory generations
generally increase the total data throughput, the latency decrease is marginal
at best.  This disparity makes it difficult to reach a high CPU utilization, which
can  presently  only  be  achieved  by  minimizing  uncached  memory  reads,  thus
avoiding the latency-bottleneck.

---

The full essay can be viewed <a href="/res/pdf/DataOrientedDesign.pdf" target="_blank"> here</a>.



