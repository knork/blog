---
layout: post
title:  "Uniform disk intersection sampling"
date: 2017-12-08
description: "Previously i used simple rejection sampling to uniformly sample the intersection area in my bokeh
implementation. Here i derive a uniform mapping function into the intersection of two disks."
tags: pbrt
draft: true
permalink: uniform-intersection-sampling.html
mathjax: true
gallery: true
---


In the [previous]({% post_url 2017-09-19-PBRT-Exercise-6-4 %}) section I described a method to simulated realistic
bokeh. In this approach I mapped the uniform points first
into the unit-disk
and then carved
the intersection out of
that distribution. As we move away from the center of the film-plane the rejection sampling naturally reduces the
amount of samples (light) passing through the lens. While this property is desirable fewer samples also means more
noise which we wish to avoid. As such we will develop two alternative approaches to samples the disk intersection
without or at least with reduced amount of rejection sampling which further allows to make the light reduction property
optional.


## Tight bounding shape

Our first approach will bound the our intersection area by a ellipse which can be uniformly sampled, though with
a higher cost than a sphere.


## Inverse distribution sampling


<figure class="left">
<a class="max-width imgbox" href="/res/photos/uniform_sampling/bokeh_1.svg"
data-imagelightbox="a">
<img  src="/res/photos/uniform_sampling/bokeh_1.svg" >
</a>
</figure>We start with two disks with positions the first will always be at origin with a radius of $1$. The second sphere
is described by a center_point $p$ and it's radius $r$.
<br style="clear:both">
<figure class="right">
<a class="max-width imgbox" href="/res/photos/uniform_sampling/bokeh_2_5.png"
data-imagelightbox="a">
<img  src="/res/photos/uniform_sampling/bokeh_2_5.png" >
</a>
</figure>

Instead of approximating our shape now we try to actually sample the shape directly we first rotate the second disk onto
the x-axis (always to the right of the first one). Of course we need to store this transformation in order to revers
it later. Thanks to our previouse transformation and the fact that the first disk is at origin with radius 1 we can
calculate the $x$-component of the *intersection* $i$ as:

$$
i_x = 1-(1-(p_x -r))
$$

Next we want the angle $\gamma_1, \gamma_2$ for our two disks

$$
\gamma_1 = \mathrm{acos}(i_x)
$$

$$
\gamma_2 = \mathrm{acos}(\frac{i_x}{r})
$$

Further we need to calculate a relative area of each segment in order to draw the right amount of samples from each
segment. For this we simply calculate the area of the circle section and subtract the triangle that the intersection
line forms with the center of the disk.
<br style="clear:both">

$$
A_1 = \theta_{max} - \mathrm{cos}(\theta_{max}) \; 2\,\mathrm{sin}(\theta{max})
$$

$$
A_2 = \underbrace{\theta_{max}\, r^2}_\mathrm{disk-section} - \underbrace{r\,\mathrm{cos}(\theta_{max}) \; \,r\,
\mathrm{sin}(\theta{max})
}_\mathrm{triangle}
$$

<figure  >
<a class="max-width imgbox" href="/res/photos/uniform_sampling/area_1.svg"
data-imagelightbox="a">
<img  src="/res/photos/uniform_sampling/area_1.svg" >
</a>
</figure>





Finally we can move each disk in it's own frame which reduces the initial problem to sampling a segment of the disk
as seen in the figure.

<figure class="right">
<a class="max-width imgbox" href="/res/photos/uniform_sampling/bokeh_3.svg"
data-imagelightbox="a">
<img  src="/res/photos/uniform_sampling/bokeh_3.svg" >
</a>
</figure>
<br style="clear:both">

### Width pdf, area cdf


### Calculate area of the segment based on $\theta$

For now we assume that $\theta \geq 0$ however we can calculate the area for $\theta < 0$ in the same manner.
We want to know the area of our segment between $\theta_{max}$ and $\theta$ . Or equivalently we could calculate
  the area between $[0, \theta]$ and subtract it from the already known area between $[0,\theta_{max}] $ which is half
   the total area of the segment, which we calculated above. And just like above we can calculate the area of $[0,
   \theta]$ by simply calculating the area of the partial disk and subtracting the (blue) triangle. As such:


$$
\definecolor{red}{RGB}{210,0,0}
\definecolor{green}{RGB}{0,130,0}
\definecolor{black}{RGB}{0,0,0}


A(\theta) = \underbrace{\frac{A_{total}}{2}}_{\mathrm{Half\; of\; the\; total \;area}} - \;\left(
\underbrace{\frac{\theta}{2}\,
r^2}_\mathrm{Disksection} - \underbrace{
\frac{1}{2}(\color{green}r\, \mathrm{cos}
(\theta_{max}) \color{black}
\cdot \color{red}
\mathrm{tan}
(\theta)\, r \, \mathrm{cos}(\theta_{max}) \color{black} )}_\mathrm{triangle} \right)
$$

<figure  >
<a class="max-width imgbox" href="/res/photos/uniform_sampling/area.svg"
data-imagelightbox="a">
<img  src="/res/photos/uniform_sampling/area.svg" >
</a>
</figure>

For $\theta < 0$ we essentially do the same thing but now we have to add onto half of the total segment as such:

$$
A(\theta) =
\begin{cases}
     \frac{1}{2} \left( A_{total} - ( \theta\, r^2 - r^2 \mathrm{cos}(\theta_{max}) \mathrm{tan}(\theta)  \right) &
     \theta \geq 0 \\
      \frac{1}{2} \left( A_{total} + ( \theta\, r^2 - r^2 \mathrm{cos}(\theta_{max}) \mathrm{tan}(\theta)  \right) &
           \theta < 0 \\
    \end{cases}
$$

### Invert the area function

We use the area function as our CDF so in order to draw samples from the distribution we have to invert $A(\theta)$.

https://www.desmos.com/calculator/nqoh0qnq61